# Install Minio

## on Macbook

Endpoint:  http://192.168.0.23:9000  http://127.0.0.1:9000                

Browser Access:
   http://192.168.0.23:9000  http://10.29.0.14:9000  http://127.0.0.1:9000                

Set alias:
```
mc config host add myminio http://127.0.0.1:9000 AP3C32XLGPC0VYJO31MW jCp2RtnLfvzr+aQvQugaAXovUG42GhrWtQcn1NKw

```

### generate certs

```
go run generate_cert.go -ca --host "192.168.0.23"
mv cert.pem ~/.minio/certs/public.crt
mv key.pem ~/.minio/certs/private.key


```
### Install

```
bash-3.2$ brew install minio/stable/minio

```

### startups with Access and Sec Keys in env't variable 

```
bash-3.2$ env | grep MINI
MINIO_SECRET_KEY=password
MINIO_ACCESS_KEY=admin

$ mstart 

Endpoint:  ......
AccessKey: admin 
SecretKey: password 

Browser Access:
   https://127.0.0.1:9000  

mc config --insecure host add myminio https://127.0.0.1:9000 admin password

```

### aws operations

```
aws --no-verify-ssl --endpoint-url https://127.0.0.1:9000 s3 ls

```

### Encryption


```
$#The following example shows how to upload an object named my-secret-diary where the content is the file ~/my-diary.txt. Note that you should use your own encryption key.

aws s3api put-object \
  --no-verify-ssl \
  --endpoint-url https://localhost:9000 \
  --bucket my-bucket --key my-secret-diary \
  --sse-customer-algorithm AES256 \
  --sse-customer-key MzJieXRlc2xvbmdzZWNyZXRrZXltdXN0cHJvdmlkZWQ= \
  --sse-customer-key-md5 7PpPLAK26ONlVUGOWlusfg== \
  --body ~/my-diary.txt 

$#In this example, a local MinIO server is running on https://localhost:9000 with a self-signed certificate. TLS certificate verification is skipped using: --no-verify-ssl. If a MinIO server uses a CA-signed certificate, then --no-verify-ssl should not be included, otherwise aws-cli would accept any certificate.

$#2.4 Display Object Information
$#Specify the correct SSE-C key of an encrypted object to display its metadata:

$ aws s3api head-object \
  --no-verify-ssl \
  --endpoint-url https://localhost:9000 \
  --bucket my-bucket \
  --key my-secret-diary \
  --sse-customer-algorithm AES256 \
  --sse-customer-key MzJieXRlc2xvbmdzZWNyZXRrZXltdXN0cHJvdmlkZWQ= \
  --sse-customer-key-md5 7PpPLAK26ONlVUGOWlusfg==

$#2.5 Download an Object
$#The following examples show how a local copy of a file can be removed and then restored by downloading it from the server:

$#Delete your local copy of my-diary.txt:

$ rm ~/my-diary.txt

$ #Restore the file by downloading it from the server:

$ aws s3api get-object \
--no-verify-ssl \
--endpoint-url https://localhost:9000 \
--bucket my-bucket \
--key my-secret-diary \
--sse-customer-algorithm AES256 \
--sse-customer-key MzJieXRlc2xvbmdzZWNyZXRrZXltdXN0cHJvdmlkZWQ= \
--sse-customer-key-md5 7PpPLAK26ONlVUGOWlusfg== \
~/my-diary.txt


```

### Amazon S3 Cloud Storage

* Create Keypair
```
bash-3.2$ awless create keypair
Please specify (TAB for completion, ','+TAB for list completion, Enter to skip optionals, Ctrl+C to quit) :
The name of the keypair to create (it will also be the name of the file stored in ~/.awless/keys):
keypair.name? minio
create keypair name=minio

Confirm (region: eu-west-2)? [y/N] y
[info]    Generating locally 4096 RSA at /Users/ANHHUY.HA/.awless/keys/minio.pem
[info]    OK create keypair (minio) 

[info]    Revert this template with `awless revert 01DA7JJZNZX5GJ2G43M9E9ZEDM`

```

* Create storage on AWS

```
bash-3.2$ awless create bucket name=java-heap
create bucket name=java-heap

Confirm (region: eu-west-2)? [y/N] y
[info]    OK create bucket (java-heap) 

[info]    Revert this template with `awless revert 01DA7KSJDDK4APKBYZSYRKHZV4`
```

* Check created object

```
bash-3.2$ awless list buckets
|   ID ▲    |                                       GRANTS                                        | CREATED |
|-----------|-------------------------------------------------------------------------------------|---------|
| java-heap | FULL_CONTROL[user:7c8b9a94a404c6f2114c8da22f6329c7f787487dda012156c0fbddc22b1738f7] | 56 mins |
|           |                                                                                     |         |

```

### S3 Gateway

Start minio gateway server for AWS S3 backend using AWS environment variables.
     NOTE: The access and secret key in this case will authenticate with MinIO instead
     of AWS and AWS envs will be used to authenticate to AWS S3.
     $ export AWS_ACCESS_KEY_ID=aws_access_key
     $ export AWS_SECRET_ACCESS_KEY=aws_secret_key
     $ export MINIO_ACCESS_KEY=$AWS_ACCESS_KEY_ID
     $ export MINIO_SECRET_KEY=$AWS_SECRET_ACCESS_KEY
     $ export MINIO_LOGGER_HTTP_ENDPOINT="http://localhost:8000/"
     $ minio gateway s3 https://localhost:9000
     $ minio gateway nas https://localhost:9001

### config minio gateways
* mc config host add mynas http://gateway-ip:9001 access_key secret_key
* mc config host add mys3 http://gateway-ip:9000 access_key secret_key

```
bash-3.2$ mc config host add mys3 https://localhost:9000 AKIAIG7C3GA5U45Z7DWA 79KeSJYe6m/Jv1wHXPwmLDHoRF06YRyLGjiMol3L
Added `mys3` successfully.
bash-3.2$ mc ls mys3
[2019-01-11 15:01:46 PST]      0B cf-templates-42f8ewlcv3v1-us-west-2/
[2019-03-01 09:28:53 PST]      0B kyriba-devops-api-trail/
[2019-03-21 08:39:53 PDT]      0B kyriba-devops-aws-logs/
[2019-04-17 14:11:27 PDT]      0B kyriba-devops-aws-logs-paris/
[2019-03-21 13:17:07 PDT]      0B kyriba-devops-flow-logs/
[2019-04-19 14:50:21 PDT]      0B kyriba-devops-paris/
[2019-03-14 04:30:27 PDT]      0B kyriba-devops-vault/
[2019-03-21 10:49:15 PDT]      0B kyriba-elk-bucket/
[2018-12-26 11:50:40 PST]      0B kyriba-poc-us-west-2-devops/
[2018-11-29 15:49:56 PST]      0B kyriba-tfstate/
[2018-12-12 06:58:41 PST]      0B kyriba-tfstate-vmware/
[2019-01-23 14:49:58 PST]      0B kyriba.kubernetes-dev/
[2019-03-19 19:30:41 PDT]      0B testbackup-kyriba/
[2019-01-04 12:22:16 PST]      0B wab-remote/

```

### Create test bucket kyriba-java-heaps
```
bash-3.2$ aws --no-verify-ssl --endpoint-url https://localhost:9000 s3api create-bucket --bucket kyriba-java-heaps

/Library/Python/2.7/site-packages/urllib3/connectionpool.py:847: InsecureRequestWarning: Unverified HTTPS request is being made. Adding certificate verification is strongly advised. See: https://urllib3.readthedocs.io/en/latest/advanced-usage.html#ssl-warnings
  InsecureRequestWarning)
{
    "Location": "/kyriba-java-heaps"
}
```

### Test clear data

```
bash-3.2$ aws s3api put-object --no-verify-ssl --endpoint-url https://localhost:9000 --bucket kyriba-java-heaps --key my-data-key --body ./my-data.txt 
/Library/Python/2.7/site-packages/urllib3/connectionpool.py:847: InsecureRequestWarning: Unverified HTTPS request is being made. Adding certificate verification is strongly advised. See: https://urllib3.readthedocs.io/en/latest/advanced-usage.html#ssl-warnings
  InsecureRequestWarning)
{
    "ETag": "\"2be449638fb8be7ef9e599a94ed4afc4\""
}

```

### Test secret data
aws s3api put-object \
  --no-verify-ssl \aws 
  --endpoint-url https://localhost:9000 \
  --bucket my-bucket --key my-secret-diary \
  --sse-customer-algorithm AES256 \
  --sse-customer-key MzJieXRlc2xvbmdzZWNyZXRrZXltdXN0cHJvdmlkZWQ= \
  --sse-customer-key-md5 7PpPLAK26ONlVUGOWlusfg== \
  --body ~/my-diary.txt 


### Create user Minio for testings

```
bash-3.2$ awless create user name=minio
create user name=minio

Confirm (region: eu-west-2)? [y/N] y
[info]    OK create user (AIDARIPC2H2GS2EEWLV6C) 

[info]    Revert this template with `awless revert 01DA8A7NP9RMG0SK7DKPESM6N7`

```

### Create access key for minio user

```
bash-3.2$ awless create accesskey user=minio
create accesskey user=minio

Confirm (region: eu-west-2)? [y/N] y
[info]    Access key created. Here are the crendentials for user minio:

****************************************************************
aws_access_key_id = AKIARIPC2H2GTVGZ7YPH
aws_secret_access_key = odd/Dd1UqnLfMWxC9AMWqZogs32/0ihxEiVUKSwe
****************************************************************

[warning] This is your only opportunity to view the secret access keys.
[warning] Save the user's new access key ID and secret access key in a safe and secure place.
[warning] You will not have access to the secret keys again after this step.

Do you want to save these access keys in /Users/ANHHUY.HA/.aws/credentials? [y/N] y
Entry profile name: (minio) 

✓ Credentials for profile 'minio' stored successfully in /Users/ANHHUY.HA/.aws/credentials

[info]    OK create accesskey (AKIARIPC2H2GTVGZ7YPH) 

[info]    Revert this template with `awless revert 01DA8AC1E0VN7YHZ90KPTB3806`

```


### Calculate md5 for secret_access_key

```
bash-3.2$ md5 -s "odd/Dd1UqnLfMWxC9AMWqZogs32/0ihxEiVUKSwe"
MD5 ("odd/Dd1UqnLfMWxC9AMWqZogs32/0ihxEiVUKSwe") = acd67e38799b03a33188ad7e8fd01bdf

```
